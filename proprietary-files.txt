# From miui_MOONSTONEGlobal_V14.0.4.0.TMPMIXM_b496aa7124_13.0
system/lib64/libcamera_algoup_jni.xiaomi.so
system/lib64/libcamera_mianode_jni.xiaomi.so
system/lib64/libmicampostproc_client.so
system/lib64/vendor.xiaomi.hardware.campostproc@1.0.so

# From missi_phoneext4_global-user 14 UKQ1.231003.002 V816.0.3.0.UMPMIXM release-keys
vendor/lib64/libHalSuperSensorServer.so
vendor/lib64/libSuperSensor.so
vendor/lib64/libSuperSensorCPU.so

# Patched MIUI Leica camera package
-product/priv-app/MiuiCamera/MiuiCamera.apk:system/priv-app/MiuiCamera/MiuiCamera.apk;OVERRIDES=Aperture,Camera,Camera2,GoogleCameraGo|1eb82ca99df9a4442133cdc6573491b8ca092740

# photo - from CRdroid repository
system/priv-app/MiuiExtraPhoto/MiuiExtraPhoto.apk;PRESIGNED
system/priv-app/MiuiExtraPhoto/lib/arm64/libdoc_photo.so
system/priv-app/MiuiExtraPhoto/lib/arm64/libdoc_photo_c++_shared.so
system/priv-app/MiuiExtraPhoto/lib/arm64/libgallery_arcsoft_dualcam_refocus.so
system/priv-app/MiuiExtraPhoto/lib/arm64/libgallery_arcsoft_portrait_lighting.so
system/priv-app/MiuiExtraPhoto/lib/arm64/libgallery_arcsoft_portrait_lighting_c.so
system/priv-app/MiuiExtraPhoto/lib/arm64/libgallery_mpbase.so
system/priv-app/MiuiExtraPhoto/lib/arm64/libmibokeh_gallery.so
system/priv-app/MiuiExtraPhoto/lib/arm64/libmisr.so
system/priv-app/MiuiExtraPhoto/lib/arm64/libmotion_photo.so
system/priv-app/MiuiExtraPhoto/lib/arm64/libmotion_photo_c++_shared.so
system/priv-app/MiuiExtraPhoto/lib/arm64/libmotion_photo_mace.so
system/priv-app/MiuiExtraPhoto/lib/arm64/librefocus.so
system/priv-app/MiuiExtraPhoto/lib/arm64/librefocus_mibokeh.so
system/priv-app/MiuiExtraPhoto/lib/arm64/libselection.so

